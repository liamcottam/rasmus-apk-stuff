package archive

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"crypto/sha1"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"io"
	"io/ioutil"
	"strings"
)

type APKVerificationData struct {
	Signature []byte
	PKGINFO   []byte
	SHA1SUM   []byte
}

// TODO: Further investigation
// This is a hack, I'm just checking for the gzip 0x1f8b header, no verification
// is done and may be buggy. Further investigation of the gzip format needed.
func getOffsets(i io.ReadSeeker) ([]int, error) {
	var (
		offset  int
		offsets []int
	)

	i.Seek(0, 0)
	reader := bufio.NewReader(i)
	for {
		bts, err := reader.ReadBytes(0x1f)
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		offset += len(bts)
		t, err := reader.Peek(1)
		if err != nil {
			return nil, err
		}
		if t[0] == 0x8b {
			offsets = append(offsets, offset-1)
			if len(offsets) > 2 {
				break
			}
		}
	}

	return offsets, nil
}

// Extracts a signature (aka .SIGN file) from the archive and a PKGINFO
// if it exists (thereby denoting it an APK archive rather than an APKINDEX).
// The .SIGN file is a PKCS signature, PKGINFO is a custom ini type format.
func getPackageData(r io.Reader) ([]byte, []byte, error) {
	var (
		signature []byte
		pkginfo   []byte
	)
	gzr, err := gzip.NewReader(r)
	if err != nil {
		return nil, nil, err
	}
	defer gzr.Close()

	tr := tar.NewReader(gzr)
	for {
		hdr, err := tr.Next()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, nil, err
		}

		if strings.HasSuffix(hdr.Name, ".pub") {
			signature, err = ioutil.ReadAll(tr)
			if err != nil {
				return nil, nil, err
			}
		} else if hdr.Name == ".PKGINFO" {
			pkginfo, err = ioutil.ReadAll(tr)
			if err != nil {
				return nil, nil, err
			}
		}

		if signature != nil && pkginfo != nil {
			break
		}
	}

	return signature, pkginfo, nil
}

// GetAPKVerificationData parses APKINDEX and .apk files and returns the
// signature contained within the file and a PKGINFO if one is contained. If a
// PKGINFO is found, further verification is performed to validate the SHA256
// sum contained within. Then it calculates the entire SHA sum which is used
// to validate the signature and SHA against an RSA Public Key.
func GetAPKVerificationData(i io.ReadSeeker) (*APKVerificationData, error) {
	var (
		result = APKVerificationData{}
		err    error
	)

	result.Signature, result.PKGINFO, err = getPackageData(i)
	if err != nil {
		return nil, err
	}

	i.Seek(0, io.SeekStart)
	offsets, err := getOffsets(i)
	if err != nil {
		return nil, err
	}

	if result.Signature == nil {
		return nil, ErrNoSignature
	}

	// PKGINFO/.apk Parsing and Validation.
	//
	// A PKGINFO file will contain a SHA256 sum for the remaining data in the
	// archive, while the PKGINFO is sha1'd to verify against a key.
	// So a .apk file contains a .SIGN region, then .PKGINFO and everything else.
	// .SIGN signs .PKGINFO and PKGINFO contains a SHA256 sum for the remaining data.
	// If it's an APKINDEX file, .SIGN signs the all of the remaining data
	var (
		buffer = make([]byte, 8096)
		end    = -1
	)
	if result.PKGINFO != nil {
		end = offsets[2] - offsets[1]
		var hash string

		for _, line := range strings.Split(string(result.PKGINFO), "\n") {
			if len(line) == 0 || line[0] == '#' {
				continue
			}
			parts := strings.Split(line, "=")
			if len(parts) != 2 {
				continue
			}

			if strings.TrimSpace(parts[0]) == "datahash" {
				hash = strings.TrimSpace(parts[1])
			}
		}

		if len(hash) == 0 {
			return nil, ErrPKGINFOMissingSHA
		}

		if _, err := i.Seek(int64(offsets[2]), io.SeekStart); err != nil {
			return nil, err
		}

		sha256hasher := sha256.New()
		for {
			n, err := i.Read(buffer)
			if err != nil {
				if err == io.EOF {
					break
				}
				return nil, err
			}

			sha256hasher.Write(buffer[:n])
		}

		if hash != hex.EncodeToString(sha256hasher.Sum(nil)) {
			return nil, ErrPKGINFODatahashMismatch
		}
	}

	// SHA1 calculation
	if _, err := i.Seek(int64(offsets[1]), io.SeekStart); err != nil {
		return nil, err
	}

	var (
		// Only used for PKGINFO/.apk files. Not really useful as the end is
		//  normally shorter than the buffer and ends up reading to overreading
		// while calculating the SHA sum.
		read       int
		sha1hasher = sha1.New()
	)
	for {
		n, err := i.Read(buffer)
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		if end != -1 {
			if read+n >= end {
				sha1hasher.Write(buffer[:end-read])
				break
			}
			read += n
		}
		sha1hasher.Write(buffer[:n])
	}

	result.SHA1SUM = sha1hasher.Sum(nil)
	return &result, nil
}

// Some generic errors
var (
	ErrNoSignature             = errors.New("no signature found")
	ErrPKGINFOMissingSHA       = errors.New("PKGINFO SHA sum missing")
	ErrPKGINFODatahashMismatch = errors.New("PKGINFO datahash mismatch")
)
