Verifies APK and .tar.gz files against keys in the ./keys dir.

You can test it by running 
```
main.exe a52dec-0.7.4-r7.apk
```

And
```
main.exe APKINDEX.tar.gz
```
