// This little package simply verifies a package against a signature.
package main

import (
	"crypto"
	"crypto/rsa"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"strings"

	"gitlab.com/liamcottam/rasmus-apk-stuff/archive"
)

type fileKey struct {
	filename string
	key      *rsa.PublicKey
}

func loadKeys() []fileKey {
	var keys = []fileKey{}

	files, err := ioutil.ReadDir("./keys/")
	if err != nil {
		panic(err)
	}
	for _, file := range files {
		filename := path.Join("./keys/", file.Name())
		k, err := ioutil.ReadFile(filename)
		if err != nil {
			panic(err)
		}

		if !strings.HasSuffix(filename, ".pub") {
			continue
		}

		block, _ := pem.Decode(k)
		if block == nil {
			panic("pem block nil")
		}
		pkixPublicKey, err := x509.ParsePKIXPublicKey(block.Bytes)
		if err != nil {
			panic(err)
		}
		rsaPublicKey, ok := pkixPublicKey.(*rsa.PublicKey)
		if !ok {
			panic("failed to cast pkix public key to *rsa.PublicKey")
		}

		keys = append(keys, fileKey{file.Name(), rsaPublicKey})
	}

	return keys
}

func verifyAPKData(data *archive.APKVerificationData, keys []fileKey) *fileKey {
	for _, v := range keys {
		if err := rsa.VerifyPKCS1v15(v.key, crypto.SHA1, data.SHA1SUM, data.Signature); err == nil {
			return &v
		}
	}
	return nil
}

func main() {
	if len(os.Args) != 2 {
		fmt.Fprintf(os.Stderr, "Missing [file] argument\n")
		os.Exit(1)
	}

	f, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	defer f.Close()

	keys := loadKeys()
	verificationData, err := archive.GetAPKVerificationData(f)
	if err != nil {
		panic(err)
	}

	fmt.Printf("Signature: %s\n", hex.EncodeToString(verificationData.Signature))
	fmt.Printf("SHA1SUM: %s\n", hex.EncodeToString(verificationData.SHA1SUM))

	key := verifyAPKData(verificationData, keys)
	if key == nil {
		fmt.Println("invalid")
		os.Exit(1)
	}

	fmt.Printf("valid: signed with %s\n", key.filename)
}
